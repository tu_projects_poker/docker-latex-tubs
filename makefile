all:
	docker build -t latex-tubs-docker .
	@echo ""
	@echo "You can now use the prepared docker image latex-tubs-docker"
	@echo "You can use the script latex-tubs-docker from this project's root directory"
	@echo "You can create a symlink in /usr/bin using the following command"
	@echo "    sudo ln -s $(shell pwd)/latex-tubs-docker /usr/bin/latex-tubs-docker"

clean:
	@echo ""
	@echo "To delete the docker image, run:"
	@echo ""
	@echo "    docker image rm latex-tubs-docker"
	@echo ""

.PHONY: all clean
