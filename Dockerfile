FROM ubuntu:19.10

ENV DEBIAN_FRONTEND noninteractive

ARG USER_NAME=latex
ARG USER_HOME=/home/latex
ARG USER_ID=1000
ARG USER_GECOS=LaTeX

RUN adduser \
  --home "$USER_HOME" \
  --uid $USER_ID \
  --gecos "$USER_GECOS" \
  --disabled-password \
  "$USER_NAME"

# Install core dependencies
RUN apt-get update && apt-get upgrade -y && \
    apt-get install -y lsb-release tzdata git git-lfs 

ARG WGET=wget
ARG GIT=git
ARG MAKE=make
ARG PANDOC=pandoc
ARG PCITEPROC=pandoc-citeproc
ARG PYGMENTS=python3-pygments
ARG FIG2DEV=fig2dev
ARG JRE=default-jre-headless

RUN apt-get install -y \
  texlive-full \
  # some auxiliary tools
  "$WGET" \
  "$GIT" \
  "$MAKE" \
  # markup format conversion tool
  "$PANDOC" \
  "$PCITEPROC" \
  # XFig utilities
  "$FIG2DEV" \
  # syntax highlighting package
  "$PYGMENTS" \
  # Java runtime environment (e.g. for arara)
  "$JRE"

RUN apt-get install -y equivs
COPY setup_files $USER_HOME/setup_files
RUN cd $USER_HOME/setup_files && \
  equivs-build latex-beamer && \
  dpkg -i latex-beamer_1.0_all.deb
RUN apt-get purge -y --auto-remove equivs

# Install ARIAL for TUBS corporate design
RUN wget -O $USER_HOME/setup_files/install-getnonfreefonts https://tug.org/fonts/getnonfreefonts/install-getnonfreefonts
RUN texlua $USER_HOME/setup_files/install-getnonfreefonts
RUN getnonfreefonts --sys --verbose arial-urw

# Install TU-BS corporate design for latex
RUN wget -O $USER_HOME/setup_files/texlive-tubs_1.1.0debian.deb http://tubslatex.ejoerns.de/1.1.0/texlive-tubs_1.1.0debian.deb
RUN dpkg -i $USER_HOME/setup_files/texlive-tubs_1.1.0debian.deb

# Clean setup files
RUN rm -R $USER_HOME/setup_files

# Removing documentation packages *after* installing them is kind of hacky,
# but it only adds some overhead while building the image.
RUN apt-get --purge remove -y .\*-doc$

# Remove more unnecessary stuff
RUN apt-get clean -y
